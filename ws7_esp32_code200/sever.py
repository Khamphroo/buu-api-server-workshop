from flask import Flask, render_template, request, abort, jsonify

app = Flask(__name__)

@app.route('/')
def index():
    return 'OK',200

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8002)
