from flask import Flask, render_template, request, abort, jsonify
import json

app = Flask(__name__)

@app.route('/')
def index():
    return 'OK',200

@app.route('/update-sensor', methods=['POST','GET'])
def updatesensor():
    if (request.method == 'POST'):
        data = request.json
        print(data)
        return 'OK',200
    elif (request.method == 'GET'):
        return 'This is GET Method', 200
    else:
        abort(400)

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8002)
