from flask import Flask, render_template, request, abort, jsonify
import json

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello Flask',200

@app.route('/get-sensor')
def getsensor():
    message = {
        'Temp': 25.5,
        'Humid': 80.45,
        'Voltage': 220.4
    }
    return jsonify(message),200

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8002)
