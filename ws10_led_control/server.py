from flask import Flask, render_template, request, abort, jsonify
from flask_cors import CORS
import json

led_status = False

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/')
def index():
    return 'OK',200

@app.route('/get-sensor')
def getsensor():
    global led_status
    
    if (request.method == 'POST'):
       return 'This is POST Method', 200
    elif (request.method == 'GET'):
        message =   {
                        'LED_STATUS': led_status,
                    }   
        return jsonify(message),200
    else:
        abort(400)

@app.route('/update-sensor', methods=['POST','GET'])
def sumdata():
    global led_status
    if (request.method == 'POST'):
        data = request.json
        led_status = data['LED_STATUS']
        return {'ststus': 'OK'},200
    elif (request.method == 'GET'):
        return 'This is GET Method', 200
    else:
        abort(400)

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8002)
